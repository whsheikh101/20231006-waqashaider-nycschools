//
//  SchoolList.swift
//  20231006-WaqasHaider-NYCSchools
//
//  Created by Waqas Haider on 10/7/23.
//

import SwiftUI

struct SchoolList: View {
    @StateObject private var viewModel = SchoolViewModel()
    
    var body: some View {
        VStack {
            switch viewModel.loadingState {
            case .loading:
                LoadingView(title: viewModel.loadingText)
                    .task { await viewModel.loadSchools() }
            case .loaded(let schools):
                NavigationSplitView {
                    List(schools, id: \.id) { school in
                        NavigationLink {
                            SchoolDetail(school: school)
                        } label: {
                            SchoolRow(school: school)
                        }
                    }
                    .listStyle(.plain)
                    .navigationTitle(viewModel.navigationTitle)
                } detail: {
                    Text(viewModel.detailScreenInitialText)
                }
            case .failed(let error):
                if let commonError = error as? CommonErrors {
                    Text(commonError.message)
                        .font(.title)
                        .padding()
                }
            }
        }
    }
}

#Preview {
    SchoolList()
}
