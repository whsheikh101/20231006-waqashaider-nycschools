//
//  _0231006_WaqasHaider_NYCSchoolsTests.swift
//  20231006-WaqasHaider-NYCSchoolsTests
//
//  Created by Waqas Haider on 10/8/23.
//

import XCTest
import Combine
@testable import _0231006_WaqasHaider_NYCSchools

final class _0231006_WaqasHaider_NYCSchoolsTests: XCTestCase {
    
    let timeout = 60.0
    var subscription: AnyCancellable?
    var schoolViewModel: SchoolViewModel?
    var schoolDetailViewModel: SchoolDetailViewModel?
    
    override func setUpWithError() throws {
        schoolViewModel = SchoolViewModel()
        schoolDetailViewModel = SchoolDetailViewModel()
    }
    
    override func tearDownWithError() throws {
        subscription = nil
        schoolViewModel = nil
        schoolDetailViewModel = nil
    }
    
    func testloadSchools() async {
        guard let unwrappedViewModel = schoolViewModel else { return }
        
        // expectation message
        let expectation = expectation(description: "Load Schools List")
        
        // call load schools API
        await unwrappedViewModel.loadSchools()
        
        // start subscription
        subscription = unwrappedViewModel.$loadingState.sink(receiveCompletion: { _ in }, receiveValue: {[weak self] loadingState in
            switch loadingState {
            case .loaded(_):
                XCTAssert(true)
                expectation.fulfill()
                
                // cancel subscription
                self?.subscription?.cancel()
            case .failed(let error):
                XCTAssert(false, (error as? CommonErrors)?.message ?? "")
                expectation.fulfill()
                
                // cancel subscription
                self?.subscription?.cancel()
            default: break
            }
        })
        
        await fulfillment(of: [expectation], timeout: timeout)
    }
    
    func testloadSchoolDetail() async {
        guard let unwrappedViewModel = schoolDetailViewModel else { return }
        
        // expectation message
        let expectation = expectation(description: "Load School Detail")
        
        // call load schools API
        await unwrappedViewModel.loadSchoolSATScore(with: "21K728")
        
        // start subscription
        subscription = unwrappedViewModel.$loadingState.sink(receiveCompletion: { _ in }, receiveValue: {[weak self] loadingState in
            switch loadingState {
            case .loaded(_):
                XCTAssert(true)
                expectation.fulfill()
                
                // cancel subscription
                self?.subscription?.cancel()
            case .failed(let error):
                XCTAssert(false, (error as? CommonErrors)?.message ?? "")
                expectation.fulfill()
                
                // cancel subscription
                self?.subscription?.cancel()
            default: break
            }
        })
        
        await fulfillment(of: [expectation], timeout: timeout)
    }
    
}
