//
//  SchoolSATScore.swift
//  20231006-WaqasHaider-NYCSchools
//
//  Created by Waqas Haider on 10/8/23.
//

import Foundation

struct SchoolSATScore: Codable {
    let id: String
    let numberOfTestTakers: String
    let criticalReadingAvgScore: String
    let writingAvgScore: String
    let mathAvgScore: String
    
    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case numberOfTestTakers = "num_of_sat_test_takers"
        case criticalReadingAvgScore = "sat_critical_reading_avg_score"
        case writingAvgScore = "sat_writing_avg_score"
        case mathAvgScore = "sat_math_avg_score"
    }
}
