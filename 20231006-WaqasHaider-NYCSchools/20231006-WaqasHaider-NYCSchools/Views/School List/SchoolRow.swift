//
//  SchoolRow.swift
//  20231006-WaqasHaider-NYCSchools
//
//  Created by Waqas Haider on 10/8/23.
//

import SwiftUI

struct SchoolRow: View {
    let school: School
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(school.name)
                    .font(.headline)
                    .fontWeight(.bold)
                Text(school.location)
                    .font(.subheadline)
                    .fontWeight(.light)
            }
            
        }
    }
}

#Preview {
    SchoolRow(school: School(id: "ID", name: "Oak Tree Road School", location: "Oak Tree Road, Iselin, NJ", overview: "school overview..."))
}
