//
//  CommonErrors.swift
//  20231006-WaqasHaider-NYCSchools
//
//  Created by Waqas Haider on 10/8/23.
//

import Foundation

enum CommonErrors: String, Error {
    case invalidRequest = "Request cannot be completed at this moment, Please try again later."
    case invalidSchoolID = "Request cannot be completed due to invalid school ID."
    case unKnown = "Something went wrong!"
    case noRecords = "No record found!"
    
    var message: String {
        return rawValue
    }
}
