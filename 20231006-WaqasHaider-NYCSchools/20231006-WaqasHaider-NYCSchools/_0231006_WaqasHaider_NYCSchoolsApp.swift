//
//  _0231006_WaqasHaider_NYCSchoolsApp.swift
//  20231006-WaqasHaider-NYCSchools
//
//  Created by Waqas Haider on 10/6/23.
//

import SwiftUI

@main
struct _0231006_WaqasHaider_NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            SchoolList()
        }
    }
}
