//
//  SchoolViewModel.swift
//  20231006-WaqasHaider-NYCSchools
//
//  Created by Waqas Haider on 10/7/23.
//

import Foundation
import Combine

class SchoolViewModel: ObservableObject {
    @Published private(set) var loadingState: LoadingState<[School]> = .loading
    
    // contants
    let loadingText = "Loading Schools..."
    let navigationTitle = "NYC High Schools."
    let detailScreenInitialText = "Select a School"
    
    @MainActor
    func loadSchools() async {
        // prepare request url
        guard let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json?$limit=100") else {
            // update state to error with information
            loadingState = .failed(CommonErrors.invalidRequest)
            return
        }
        
        do {
            // load data
            let (data, _) = try await URLSession.shared.data(from: url)
            
            // decode response
            if let schools = try? JSONDecoder().decode([School].self, from: data) {
                print("Schools: \(schools)")
                
                // update state to loaded with Schools Array
                loadingState = .loaded(schools)
            } else {
                // update state to error with information
                loadingState = .failed(CommonErrors.noRecords)
            }
        } catch(let error) {
            // update state to error with information
            loadingState = .failed(error)
        }
    }
}
