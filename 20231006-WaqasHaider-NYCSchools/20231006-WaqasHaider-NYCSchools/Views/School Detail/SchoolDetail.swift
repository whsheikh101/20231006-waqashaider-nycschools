//
//  SchoolDetail.swift
//  20231006-WaqasHaider-NYCSchools
//
//  Created by Waqas Haider on 10/8/23.
//

import SwiftUI

struct SchoolDetail: View {
    @StateObject private var viewModel = SchoolDetailViewModel()
    private let school: School
    
    init(school: School) {
        self.school = school
    }
    
    var body: some View {
        VStack {
            switch viewModel.loadingState {
            case .loading:
                LoadingView(title: viewModel.loadingText)
                    .task { await viewModel.loadSchoolSATScore(with: school.id) }
            case .loaded(let schoolSATScore):
                ScrollView {
                    VStack {
                        Text(school.name)
                            .font(.largeTitle)
                            .multilineTextAlignment(.center)
                        Text("\(school.location)")
                            .font(.headline)
                            .fontWeight(.light)
                            .multilineTextAlignment(.center)
                    }
                    Spacer()
                        .frame(height: 30)
                    VStack(alignment: .leading) {
                        Text(viewModel.satText)
                            .font(.title2)
                        VStack(alignment: .leading) {
                            HStack {
                                Text(viewModel.numberOfStudentTakenTestText)
                                    .bold()
                                Text(schoolSATScore.numberOfTestTakers)
                            }
                            HStack {
                                Text(viewModel.readingAverageScoreText)
                                    .bold()
                                Text(schoolSATScore.criticalReadingAvgScore)
                            }
                            HStack {
                                Text(viewModel.writingAverageScoreText)
                                    .bold()
                                Text(schoolSATScore.writingAvgScore)
                            }
                            HStack {
                                Text(viewModel.mathAverageScoreText)
                                    .bold()
                                Text(schoolSATScore.mathAvgScore)
                            }
                        }
                        Spacer()
                            .frame(height: 20)
                        VStack(alignment: .leading) {
                            Text(viewModel.overviewText)
                                .bold()
                            Text(school.overview)
                                .font(.callout)
                        }
                    }
                }
                .padding()
                
            case .failed(let error):
                if let commonError = error as? CommonErrors {
                    Text(commonError.message)
                        .font(.title)
                        .padding()
                }
            }
        }
    }
}

#Preview {
    SchoolDetail(school: School(id: "21K728", name: "Oak Tree Road School", location: "Oak Tree Road, Iselin, NJ", overview: "school overview..."))
}
