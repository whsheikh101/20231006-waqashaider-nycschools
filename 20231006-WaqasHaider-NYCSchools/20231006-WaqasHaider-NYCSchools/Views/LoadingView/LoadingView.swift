//
//  LoadingView.swift
//  20231006-WaqasHaider-NYCSchools
//
//  Created by Waqas Haider on 10/8/23.
//

import SwiftUI

struct LoadingView: View {
    var title: String
    
    var body: some View {
        ProgressView()
            .controlSize(.large)
        Text(title)
            .font(.title)
    }
}

#Preview {
    LoadingView(title: "Loading...")
}
