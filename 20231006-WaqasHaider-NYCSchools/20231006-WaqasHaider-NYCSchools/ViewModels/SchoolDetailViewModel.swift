//
//  SchoolDetailViewModel.swift
//  20231006-WaqasHaider-NYCSchools
//
//  Created by Waqas Haider on 10/8/23.
//

import Foundation
import Combine

class SchoolDetailViewModel: ObservableObject {
    @Published private(set) var loadingState: LoadingState<SchoolSATScore> = .loading
    
    // contants
    let loadingText = "Loading School Detail..."
    let satText = "SAT Score:"
    let numberOfStudentTakenTestText = "Number of student taken test:"
    let readingAverageScoreText = "Reading Averge Score:"
    let writingAverageScoreText = "Writing Averge Score:"
    let mathAverageScoreText = "Math Averge Score:"
    let overviewText = "Overview:"
    
    @MainActor
    func loadSchoolSATScore(with schoolID: String) async {
        // prepare request url
        guard !schoolID.isEmpty else {
            // update state to error with information
            loadingState = .failed(CommonErrors.invalidSchoolID)
            return
        }
        guard let url = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=\(schoolID)") else {
            // update state to error with information
            loadingState = .failed(CommonErrors.invalidRequest)
            return
        }
        
        do {
            // load data
            let (data, _) = try await URLSession.shared.data(from: url)
            
            // decode response
            if let schoolSATScores = try? JSONDecoder().decode([SchoolSATScore].self, from: data), let SATScore = schoolSATScores.first {
                print("SchoolSATScore: \(SATScore)")
                
                // update state to loaded with SAT score
                loadingState = .loaded(SATScore)
            } else {
                // update state to error with information
                loadingState = .failed(CommonErrors.noRecords)
            }
        } catch(let error) {
            // update state to error with information
            loadingState = .failed(error)
        }
    }
}
