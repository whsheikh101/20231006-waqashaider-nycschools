//
//  School.swift
//  20231006-WaqasHaider-NYCSchools
//
//  Created by Waqas Haider on 10/7/23.
//

import Foundation

enum LoadingState<Value> {
    case loading
    case failed(Error)
    case loaded(Value)
}

struct School: Codable, Identifiable {
    let id: String
    let name: String
    let location: String
    let overview: String
    
    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case name = "school_name"
        case location
        case overview = "overview_paragraph"
    }
}
